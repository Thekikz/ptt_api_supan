var http = require('http'),
    path = require('path'),
    methods = require('methods'),
    express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    cors = require('cors'),
    passport = require('passport'),
    errorhandler = require('errorhandler'),
    mongoose = require('mongoose');


var isProduction = process.env.NODE_ENV === 'production';

// Create global app object
var app = express();

app.use(cors());

// Normal express config defaults
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(require('method-override')());
// app.use(express.static(__dirname + '/public'));
// app.use('/static', express.static(path.join(__dirname, 'public')))
app.use('/static', express.static('g_drive'))

app.use(session({ secret: 'deaware', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false  }));

if (!isProduction) {
  app.use(errorhandler());
}


 
 mongoose.connect("mongodb://128.199.247.187:27017/PTT_PUMP", { auth:{

   authdb: "admin",
   user: 'admin',
   password: 'deaw1234'

 }}).then(function(db){  
 });
// mongoose.connect('mongodb://localhost/PTT_PUMP');
// mongoose.set('debug', true);
mongoose.set('useNewUrlParser', false);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', false);
mongoose.set('useUnifiedTopology', false);

require('./models/User');

require('./models/User');
require('./models/Sensor');
require('./models/Station');
require('./models/EchoFile');
require('./models/EchoRaw');
require('./models/RTUStatus');

require('./config/passport');

app.use(require('./routes'));

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (!isProduction) {
  app.use(function(err, req, res, next) {
    console.log(err.stack);

    res.status(err.status || 500);

    res.json({'errors': {
      message: err.message,
      error: err
    }});
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({'errors': {
    message: err.message,
    error: {}
  }});
});

// finally, let's start our server...
var server = app.listen( process.env.PORT || 4000, function(){
  console.log('Listening on port ' + server.address().port);
});
