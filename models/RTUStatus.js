var mongoose = require('mongoose');

var RTUSchema = new mongoose.Schema({
    sid:String,
    send_time:String
},{ collection: 'rtu_status',timestamps: true });

mongoose.model('RTUStatus', RTUSchema);