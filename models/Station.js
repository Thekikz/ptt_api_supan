var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
    station_id:String, 

    dec_PT:String,
    dec_TT:String,
    dec_Load_cell:String,
    dec_Gas_Gun:String,
    dec_FT:String,
    dec_POS:String,
    // dec_willpilot:String่,

    type_PT:String,
    type_TT:String,
    type_Load_cell:String,
    type_Gas_Gun:String,
    type_FT:String,
    type_POS:String,
    // type_willpilot:String,

    tag_PT:String,
    tag_TT:String,
    tag_Load_cell:String,
    tag_Gas_Gun:String,
    tag_FT:String,
    tag_POS:String,
    // tag_willpilot:String,

    url_grafana:String,

    main_page:String,
    main_sum_page:String,
    img:String,
});
mongoose.model('Station', SensorSchema);