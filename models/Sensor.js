var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
    station_id:String, 
    PT:String,
    TT:String,
    Load_cell:String,
    Gas_Gun:String,
    FT:String,
    PU_status:String,
    POS:String,
    // sensor_willpilot:String,
    timestamp_s: String,
    timestamp_g: String,
});

mongoose.model('Sensor', SensorSchema);