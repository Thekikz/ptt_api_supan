var router = require('express').Router();
var mongoose = require('mongoose');
// var Field_sensor = mongoose.model('Field_sensor');
var Sensor = mongoose.model('Sensor');
var Station = mongoose.model('Station');
// var Device = mongoose.model('Device')

router.post('/find/:station',function(req, res, next){
    // req.params.station
    var qry = {};
    qry.station_id = req.params.station;
    Promise.all([      
        Sensor.find(qry).sort({_id:-1}).limit(1),
        Station.find(qry)
    ]).then(function(result){
        var out = {}
        out.dec = result[1];
        out.sesnor = result[0]
        return res.json(out)        

    }).catch(next);
})

router.put('/update', (req, res, next) => {
    Promise.all([ 
        Sensor.findOne({"station_id":req.body.station_id}),
    ]).then(function(result){
        var tmp_msg  = result[0];
        var date = new Date();
        tmp_msg.timestamp_s = date
        if(typeof req.body.PT !== 'undefined'){
            tmp_msg.PT = req.body.PT;
        }
        if(typeof req.body.TT !== 'undefined'){
            tmp_msg.TT = req.body.TT;
        }
        if(typeof req.body.FT !== 'undefined'){
            tmp_msg.FT = req.body.FT;
        }
        if(typeof req.body.Load_cell !== 'undefined'){
            tmp_msg.Load_cell = req.body.Load_cell;
        }
        if(typeof req.body.Gas_Gun !== 'undefined'){
            tmp_msg.Gas_Gun = req.body.Gas_Gun;
        }
        if(typeof req.body.POS !== 'undefined'){
            tmp_msg.POS = req.body.POS;
        }
        if(typeof req.body.timestamp !== 'undefined'){
            tmp_msg.timestamp_g = req.body.timestamp;
        }
        // "station_id": '3',
        // "PT":0,
        // "TT":0,
        // "FT":0,
        // "Load_cell":0,
        // "Gas_Gun":0,
        // "POS":0,
        // "timestamp": 'Tue Mar 24 2020 10:42:36 GMT+0700 (GMT+07:00)'
        tmp_msg.save().then(function(err){
            //  - console.log(err)
            return res.json(tmp_msg);
        })  
    }).catch(next);
});

router.post('/save', function(req, res, next) {

    var sensor_new = new Sensor();

    sensor_new.station_id = req.body.station_id

    var date = new Date();
    sensor_new.PT = req.body.PT
    sensor_new.TT = req.body.TT
    sensor_new.Load_cell = req.body.Load_cell
    sensor_new.Gas_Gun = req.body.Gas_Gun
    sensor_new.FT = req.body.FT
    sensor_new.PU_status = req.body.PU_status
    sensor_new.timestamp_s = date
    sensor_new.timestamp_g = req.body.timestamp
    sensor_new.POS = req.body.POS

    sensor_new.save().then(function(){
        return res.json({
            mes: "Seccess"
        });
    }).catch(next);
});

module.exports = router;