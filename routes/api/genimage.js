const puppeteer = require('puppeteer');
var router = require('express').Router();

router.get('/:sid', function(req, res, next) {
    //  - console.log(req.params.sid);
    if (req.params.sid === undefined){
        return res.json({msg: "missing : 'sid'"})
    }
    // var station_name = ''
    // if (req.params.sid === '1'){
    //     station_name = "NPI-B04";
    // }
    // if (req.params.sid === '2'){
    //     station_name = "NPI-A06";
    // }
    // if (req.params.sid === '3'){
    //     station_name = "NPI-C01";
    // }
    // station_name = req.params.sid
    (async () => {
        // const browser = await puppeteer.launch({ignoreDefaultArgs: ['--disable-extensions']});
        const browser = await puppeteer.launch({
            args: ["--no-sandbox", "--disable-setuid-sandbox"]
        });
        const page = await browser.newPage();
        var d_time = new Date();

        await page.goto('https://www.wellmonitors.site/api/Gchart.php?index=' + req.params.sid);
        await page.setViewport({
            width: 800,
            height: 800,
            deviceScaleFactor: 1,
        }); 
        var image_name = req.params.sid +"_"+d_time.getDate() + "-" + d_time.getMonth() + "-" + d_time.getFullYear() + "_" + d_time.getHours()  + "_" + d_time.getMinutes() + "_" + d_time.getSeconds()+ ".png"
        await page.screenshot({path: "/var/www/html/pttpump/line_image/" + image_name });
        await browser.close();
        return res.json({msg: "ok", station_name: req.params.sid,image_name : image_name})
    })();
});

module.exports = router;
