var router = require('express').Router();
var mongoose = require('mongoose');
var RTUStatus = mongoose.model('RTUStatus');

router.get('/:sid', function(req, res, next) {
    //  - console.log(req.params.sid);

    var qry = {sid:req.params.sid}
    Promise.all([
        RTUStatus.find(qry)
    ]).then(function(result){
	    //  - console.log(result[0]);
        
        return res.json(result[0])
    }).catch(next);
});

router.post('/:sid', function(req, res, next) {
    var filter = { sid: req.params.sid};
    //  - console.log(req.params.sid);

    RTUStatus.findOneAndUpdate(filter, {$set:{send_time:"0"}}, {new: true}, (err, doc) => {
        if (err) {
            //  - console.log("Something wrong when updating data!");
            next()
        }
        //  - console.log(doc);

        if(doc===null){
            var rtu_status = new RTUStatus({  sid: req.params.sid,send_time:"0"});
 
            // save model to database
            rtu_status.save(function (err, book) {
            if (err) return console.error(err);
                //  - console.log(" saved new collection.");
            });
        }
        
        res.json(doc)
    });
    
    
});

module.exports = router;