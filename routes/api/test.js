const puppeteer = require('puppeteer');
async function main() {
  const browser = await puppeteer.launch({
    args: ["--no-sandbox",
		"--disable-setuid-sandbox"]
  });
  const page = await browser.newPage();
  await page.goto('http://wellmonitors.site/api/Gchart.php?index=3');
  await page.screenshot({
  path: 'example.png'
  });
  await browser.close();
}

// Start the script
main();