var router = require('express').Router();
var mongoose = require('mongoose');
var Sensor = mongoose.model('Sensor');
var Station = mongoose.model('Station');

var auth = require('../auth');

router.post('/find/:station',function(req, res, next){
    var qry = {};
    qry.sid = req.params.station;
    Promise.all([
        Station.find(qry)
    ]).then(function(result){
	//  - console.log(result);
        var out = {}
        out.dec = result[0];
        return res.json(out)
    }).catch(next);
})

// api call station all
router.post('/finds/all', auth.required, function(req, res, next){
    //  - console.log(req.payload);
    // req.params.station
    //  - console.log(req.payload.exp - req.payload.iat)
    if ((req.payload.exp - req.payload.iat) < 0){
        return res.json({
            mes: "Please authentication.",
            status_select: false
        })
    } else {
        var qry = {};
        qry.station_id = {};
        Promise.all([
            Station.find().sort({main_sum_page: 1})
        ]).then(function(result){
            var out = {}
            out.dec = result;
            out.mes = "OK";
            out.status = true
            return res.json(out)
        }).catch(next);
    }
})
// ---------------------------------------------------------------




router.post('/save', auth.required, function(req, res, next) {
    if ((req.payload.exp - req.payload.iat) < 0){
        return res.json({
            mes: "Please authentication.",
            status_select: false
        })
    } else {
        // check 
        var qry = {};
        qry.station_id = req.body.station_id;
        station_id = req.body.station_id;
        Promise.all([
            Station.find(qry).count()
        ]).then(function(result){
            //  - console.log(result[0])
            if (result[0] === 0){
                var station_new = new Station();
                //for id
                station_new.station_id = req.body.station_id

                //for description
                station_new.dec_PT = req.body.dec_PT
                station_new.dec_TT = req.body.dec_TT
                station_new.dec_Load_cell = req.body.dec_Load_cell
                station_new.dec_Gas_Gun = req.body.dec_Gas_Gun
                station_new.dec_FT = req.body.dec_FT
                station_new.dec_POS = req.body.dec_POS
                //for type
                station_new.type_PT = req.body.type_PT
                station_new.type_TT = req.body.type_TT
                station_new.type_Load_cell = req.body.type_Load_cell
                station_new.type_Gas_Gun = req.body.type_Gas_Gun
                station_new.type_FT = req.body.type_FT
                station_new.type_POS = req.body.type_POS

                //for tag
                station_new.tag_PT = req.body.tag_PT
                station_new.tag_TT = req.body.tag_TT
                station_new.tag_Load_cell = req.body.tag_Load_cell
                station_new.tag_Gas_Gun = req.body.tag_Gas_Gun
                station_new.tag_FT = req.body.tag_FT
                station_new.tag_POS = req.body.tag_POS

                station_new.url_grafana = req.body.url_grafana


                station_new.main_page = req.body.main_page
                station_new.main_sum_page = req.body.main_sum_page
                station_new.img = req.body.img

                station_new.save().then(function(){
                    return res.json({
                        mes: "Seccess",
                        status_insert: true
                    });
                }).catch(next);
            } else {
                return res.json({
                    mes: 'station id "' + station_id + '" available.',
                    status_insert: false
                });
            }
        }).catch(next);
    }
    
});

router.put('/', auth.required, function(req, res, next){
    if ((req.payload.exp - req.payload.iat) < 0){
        return res.json({
            mes: "Please authentication.",
            status_select: false
        })
    } else {
        station_id = req.body.station_id;
        var qry = {};
        qry.station_id = station_id;
        Promise.all([ 
            Station.findOne({"station_id":req.body.station_id}),    // result 0
            Station.find(qry).count()                               // result 1
        ]).then(function(result){
    
            if (result[1] === 1){
                var tmp_msg  = result[0];
            
                // for type
                if(typeof req.body.type_FT !== 'undefined'){
                    tmp_msg.type_FT = req.body.type_FT;
                }
        
                if(typeof req.body.type_Gas_Gun !== 'undefined'){
                    tmp_msg.type_Gas_Gun = req.body.type_Gas_Gun;
                }
        
                if(typeof req.body.type_Load_cell !== 'undefined'){
                    tmp_msg.type_Load_cell = req.body.type_Load_cell;
                }
                
                if(typeof req.body.type_TT !== 'undefined'){
                    tmp_msg.type_TT = req.body.type_TT;
                }
        
                if(typeof req.body.type_PT !== 'undefined'){
                    tmp_msg.type_PT = req.body.type_PT;
                }
                if(typeof req.body.type_POS !== 'undefined'){
                    tmp_msg.type_POS = req.body.type_POS;
                }
                // if(typeof req.body.type_willpilot !== 'undefined'){
                //     tmp_msg.type_willpilot = req.body.type_willpilot;
                // }
        
                // for dec
                if(typeof req.body.dec_FT !== 'undefined'){
                    tmp_msg.dec_FT = req.body.dec_FT;
                }
        
                if(typeof req.body.dec_Gas_Gun !== 'undefined'){
                    tmp_msg.dec_Gas_Gun = req.body.dec_Gas_Gun;
                }
        
                if(typeof req.body.dec_Load_cell !== 'undefined'){
                    tmp_msg.dec_Load_cell = req.body.dec_Load_cell;
                }
        
                if(typeof req.body.dec_TT !== 'undefined'){
                    tmp_msg.dec_TT = req.body.dec_TT;
                }
        
                if(typeof req.body.dec_PT !== 'undefined'){
                    tmp_msg.dec_PT = req.body.dec_PT;
                }
                if(typeof req.body.dec_POS !== 'undefined'){
                    tmp_msg.dec_POS = req.body.dec_POS;
                }
                // if(typeof req.body.dec_willpilot !== 'undefined'){
                //     tmp_msg.dec_willpilot = req.body.dec_willpilot;
                // }
        
                // for tag
                if(typeof req.body.tag_FT !== 'undefined'){
                    tmp_msg.tag_FT = req.body.tag_FT;
                }
        
                if(typeof req.body.tag_Gas_Gun !== 'undefined'){
                    tmp_msg.tag_Gas_Gun = req.body.tag_Gas_Gun;
                }
        
                if(typeof req.body.tag_Load_cell !== 'undefined'){
                    tmp_msg.tag_Load_cell = req.body.tag_Load_cell;
                }
        
                if(typeof req.body.tag_TT !== 'undefined'){
                    tmp_msg.tag_TT = req.body.tag_TT;
                }
        
                if(typeof req.body.tag_PT !== 'undefined'){
                    tmp_msg.tag_PT = req.body.tag_PT;
                }
                if(typeof req.body.tag_POS !== 'undefined'){
                    tmp_msg.tag_POS = req.body.tag_POS;
                }
                // if(typeof req.body.tag_willpilot !== 'undefined'){
                //     tmp_msg.tag_willpilot = req.body.tag_willpilot;
                // }
                
                // url grafana
                if(typeof req.body.url_grafana !== 'undefined'){
                    tmp_msg.url_grafana = req.body.url_grafana;
                }
                if(typeof req.body.main_page !== 'undefined'){
                    tmp_msg.main_page = req.body.main_page;
                }
                if(typeof req.body.main_sum_page !== 'undefined'){
                    tmp_msg.main_sum_page = req.body.main_sum_page;
                }
                if(typeof req.body.img !== 'undefined'){
                    tmp_msg.img = req.body.img;
                }
            
                tmp_msg.save().then(function(err){
                    //  - console.log(err)
                    return res.json({
                        data: tmp_msg,
                        mes: 'updated station.',
                        status_insert: true
                    });
                })  
            } else {
                return res.json({
                    mes: 'station id "' + station_id + '" unavailable. Please insert station',
                    status_insert: false
                });
            }
    
        }).catch(next);
    }

});

router.delete('/', auth.required,function(req, res, next){

    if ((req.payload.exp - req.payload.iat) < 0){
        return res.json({
            mes: "Please authentication.",
            status_select: false
        })
    } else {
        station_id = req.body.station_id;
        var qry = {};
        qry.station_id = station_id;
        Promise.all([ 
            Station.findOneAndRemove(qry)                               // result 0
        ]).then(function(result){
            //  - console.log(result[0])
            if (result[0] !== null){
                return res.json({
                    mes: "station id " + station_id + " deleted.",
                    result: result,
                    status: true
                })
            } else {
                return res.json({
                    mes: 'station id "' + station_id + '" unavailable. Please insert station',
                    result: result,
                    status: false
                })
            }
        });
    }

});

module.exports = router;
